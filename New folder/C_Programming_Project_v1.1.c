//This is a students' database

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef unsigned int      uint8;

/*************** Database Module ***************/

struct SimpleDB
{
    struct SimpleDB * prev;
    int Student_ID, Student_year, count;
    uint8 Course_1[2]; /*element 0 holds ID and element 1 holds grade*/
    uint8 Course_2[2];
    uint8 Course_3[2];
    struct SimpleDB * next;
};

struct SimpleDB *pHead = NULL;
struct SimpleDB *pTail = NULL;

/************ Functions Prototypes ************/

bool SDB_IsFull(void);
uint8 SDB_GetUsedSize(void);
bool SDB_AddEntry(uint8 id, uint8 year, uint8* subjects, uint8* grades);
void SDB_DeleteEntry(uint8 id);
bool SDB_ReadEntry(uint8 id, uint8* year, uint8* subjects, uint8* grades);
void SDB_GetIdList(uint8* count, uint8* list);
bool SDB_IsIdExist(uint8 ID);



void main()
{
    uint8 list[10];
    uint8 grades[3],subjects[3];
    uint8 id, year, count;
    char ans;
    for(int i=0;i<10;i++)
    {
        list[i]=0;
    }
    printf("Welcome to Students' Simple Database Module.\n");
    do{
        printf("Enter Student's ID followed by Year: ");
        scanf("%d %d",&id, &year);
        for(int i=0;i<3;i++)
        {
            printf("Please enter student's Subject %d ID and Grade: ",i+1);
            scanf("%d %d", &subjects[i],&grades[i]);
        }
        if(SDB_AddEntry(id, year, subjects, grades))
            printf("Values has been successfully added!\n");
        else
            printf("\rAdding new values failed!\n ");

        printf("\rDo you want to Enter another value?(y/n): ");
        fflush(stdin);
        scanf("%c",&ans);


    }while(!(SDB_IsFull())&&(ans=='y'));

    if((SDB_IsFull()))
    {
        printf("Database is FULL!!\n");
    }

    printf("Do you want to get used Size?(y/n) ");
    fflush(stdin);
    scanf("%c",&ans);

    if(ans=='y')
    {
        printf("Database used size is: %d\n",SDB_GetUsedSize());
    }

    printf("Do you want to delete an entry?(y/n) ");
    fflush(stdin);
    scanf("%c",&ans);

    if(ans=='y')
    {
        printf("Enter Student's ID you want to delete: ");
        scanf("%d",&id);
        SDB_DeleteEntry(id);
    }

    SDB_GetIdList(&count, list);
    if(count != 0)
        printf("You have entered %d IDs and they're:\n",count);
    else
        printf("Your Database is Empty, Exiting....\n");

    uint8 i=0;
    while(list[i]!=0)
    {
        printf("%d\n",list[i]);
        i++;
    }
    if(count != 0)
    {
        printf("Do you want to find a specific student by his/her ID?(y/n) ");
        fflush(stdin);
        scanf("%c",&ans);

        if(ans=='y')
        {
            printf("Please Enter student's ID: ");
            scanf("%d",&id);
            if((SDB_IsIdExist(id)))
                printf("ID %d Exists!\n",id);
            else
                printf("ID %d doesn't exist!\n",id);
        }

        printf("Do you want to read a specific student's ID info?(y/n) ");
        fflush(stdin);
        scanf("%c",&ans);

        if(ans=='y')
        {
            printf("Please enter student's ID: ");
            scanf("%d",&id);

            if((SDB_ReadEntry(id,&year,subjects,grades)))
            {
                printf("ID %d found, it's data is:\n",id);
                printf("Year: %d\n",year);
                for(int i=0;i<3;i++)
                {
                    printf("Course ID: %d   Grade: %d\n",subjects[i],grades[i]);
                }
            }else
                printf("ID %d not found!\n",id);
        }
    }
}

//End of Main

/**************************************************************************/
/************************* Functions Definitions **************************/
/**************************************************************************/


/********* Checking if Database is FULL *********/

bool SDB_IsFull(void)
{
   if(pTail != NULL)
   {
     if(pTail->count == 10)
        return 1;
   else
        return 0;
   }else
        return 0;
}

/********* Checking number of Entries *********/

uint8 SDB_GetUsedSize(void)
{
    if(pTail == NULL)
        return 0;
    else
        return(pTail->count);
}

/********* Adding Entries to DB *********/

bool SDB_AddEntry(uint8 id, uint8 year, uint8* subjects, uint8* grades)
{
    if(pHead == NULL)
    {
        for(int i=0; i<3;i++)
        {
            if(!((grades[i]<=100) && (grades[i]>=0)))
            {
                printf("\rYou have entered a wrong grade!\n");
                return 0;
            }
        }

        pHead                = (struct SimpleDB*)malloc(sizeof(struct SimpleDB));
        if(pHead != NULL)
        {
            pHead->Student_ID    = id;
            pHead->Student_year  = year;
            pHead->count         = 1; //counts entry no.
            pHead->Course_1[0]   = subjects[0];
            pHead->Course_2[0]   = subjects[1];
            pHead->Course_3[0]   = subjects[2];
            pHead->prev          = NULL;
            pHead->next          = NULL;
            pTail                = pHead;
            pTail->Course_1[1]   = grades[0];
            pTail->Course_2[1]   = grades[1];
            pTail->Course_3[1]   = grades[2];
            return 1;
        }else
            printf("\n\rNo Memory Available!\n");
    }
    else
    {
        if(pTail->count < 10)
        {
            if(SDB_IsIdExist(id))
            {
                printf("ID already exists!");
                return 0;
            }
            for(int i=0; i<3;i++)
            {
                if(!((grades[i]<=100) && (grades[i]>=0)))
                {
                    printf("\rYou have entered a wrong grade!\n");
                    return 0;
                }
            }
            struct SimpleDB* temp = (struct SimpleDB*)malloc(sizeof(struct SimpleDB));
            if(temp != NULL)
            {
                temp->prev            = pTail;
                temp->count           = pTail->count + 1; //counts entry no.
                pTail->next           = temp;
                pTail                 = temp;
                pTail->next           = NULL;
                pTail->Student_ID     = id;
                pTail->Student_year   = year;
                pTail->Course_1[0]    = subjects[0];
                pTail->Course_2[0]    = subjects[1];
                pTail->Course_3[0]    = subjects[2];
                pTail->Course_1[1]    = grades[0];
                pTail->Course_2[1]    = grades[1];
                pTail->Course_3[1]    = grades[2];

                return 1;
            }else
                printf("\r\nNo Memory Available!!!\n");

        }else
            return 0;
    }
}


/********* Deleting an Entry *********/


void SDB_DeleteEntry(uint8 id)
{
    struct SimpleDB* temp = pHead;
    do
    {
        if(temp->Student_ID == id)
        {
            if(temp->next == NULL)
            {
                if(temp->prev == NULL)
                {
                    pHead = NULL;
                    pTail = NULL;
                    free(temp);
                }
                else
                {
                    pTail = temp->prev;
                    free(temp);
                }
                printf("\nEntry whose ID is %d has been deleted!\n",id);
                break;
            }
            else
            {
                temp->next->prev = temp->prev;
                temp->prev->next = temp->next;
                pTail->count     = pTail->count - 1; /*can be replaced by a loop
                                                 decreasing the count in
                                                 succeeding entries of deleted entry.*/
                free(temp);
                printf("\nEntry whose ID is %d has been deleted!\n",id);
                break;
            }
        }
        temp = temp->next;
    }while(temp != NULL);
}


/********* Reading Data for a given ID *********/


bool SDB_ReadEntry(uint8 id, uint8* year, uint8* subjects, uint8* grades)
{
    struct SimpleDB* temp = pHead;
    do
    {
        if(temp->Student_ID == id)
        {
            *year       =   temp->Student_year;
            subjects[0] =   temp->Course_1[0];
            subjects[1] =   temp->Course_2[0];
            subjects[2] =   temp->Course_3[0];
            grades[0]   =   temp->Course_1[1];
            grades[1]   =   temp->Course_2[1];
            grades[2]   =   temp->Course_3[1];

            return 1;
            break;
        }
        temp = temp->next;
    }while(temp != NULL);
    if (temp == NULL)
    {
        return 0;
    }
}

/********* Getting list of IDs & no. of entries *********/

void SDB_GetIdList(uint8* count, uint8* list)
{
    if(pTail != NULL)
    {

        *count                = pTail->count;
        struct SimpleDB* temp = pHead;
        uint8 i               = 0;

        do
        {
            list[i] =   temp->Student_ID;
            temp    =   temp->next;
            i++;
        }while(temp != NULL);
    }else
    {
        *count  = 0;
        list[0] = 0;
    }
}


/********* Checking if given ID already exists! *********/


bool SDB_IsIdExist(uint8 ID)
{
    struct SimpleDB* temp = pHead;
    do
    {
        if(temp->Student_ID == ID)
        {
            return 1;
            break;
        }
        temp = temp->next;
    }while(temp != NULL);
    if (temp == NULL)
    {
        return 0;
    }
}
